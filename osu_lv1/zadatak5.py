""" Zadatak 1.4.5 Napišite Python skriptu koja ce u ´ citati tekstualnu datoteku naziva ˇ SMSSpamCollection.txt
[1]. Ova datoteka sadrži 5574 SMS poruka pri cemu su neke ozna ˇ cene kao ˇ spam, a neke kao ham.
Primjer dijela datoteke:
ham Yup next stop.
ham Ok lar... Joking wif u oni...
spam Did you hear about the new "Divorce Barbie"? It comes with all of Ken’s stuff!
a) Izracunajte koliki je prosje ˇ can broj rije ˇ ci u SMS porukama koje su tipa ham, a koliko je ˇ
prosjecan broj rije ˇ ci u porukama koje su tipa spam. ˇ
b) Koliko SMS poruka koje su tipa spam završava usklicnikom ?  """

file = open("SMSSpamCollection.txt", encoding="utf8")

num_ham_msgs = 0
num_spam_msgs = 0
total_ham_words = 0
total_spam_words = 0
num_spam_exclamation = 0

for line in file:
    line = line.strip()

    if line.startswith('ham'):
        num_ham_msgs += 1
        total_ham_words += len(line.split())
    
    elif line.startswith('spam'):
        num_spam_msgs += 1
        total_spam_words += len(line.split())
        
        if line.endswith('!'):
            num_spam_exclamation += 1

avg_ham_words = total_ham_words / num_ham_msgs
avg_spam_words = total_spam_words / num_spam_msgs

print("Prosječan broj riječi u ham porukama: {:.2f}".format(avg_ham_words))
print("Prosječan broj riječi u spam porukama: {:.2f}".format(avg_spam_words))
print("Broj spam poruka koje završavaju uskličnikom: {}".format(num_spam_exclamation))