""" Napišite Python skriptu koja ce u ´ citati tekstualnu datoteku naziva ˇ song.txt.
Potrebno je napraviti rjecnik koji kao klju ˇ ceve koristi sve razli ˇ cite rije ˇ ci koje se pojavljuju u ˇ
datoteci, dok su vrijednosti jednake broju puta koliko se svaka rijec (klju ˇ c) pojavljuje u datoteci. ˇ
Koliko je rijeci koje se pojavljuju samo jednom u datoteci? Ispišite ih. """

text = open("song.txt", "r")

d = dict()

for line in text:
    line = line.strip()
    line = line.lower()
    line = line.replace(",", "")
    words = line.split(" ")
    for word in words:
        if word in d:
            d[word] = d[word] + 1
        else:
            d[word] = 1
  
counter=0
print("Pobrojanje svih ponavljanja riječi u pjesmi: ")
for key in list(d.keys()):
    print(key, ":", d[key])
    if d[key]==1:
        counter+=1

print("Broj jedinstvenih riječi od po jedno ponavljanje je: "+ str(counter))
