""" Zadatak 1.4.3 Napišite program koji od korisnika zahtijeva unos brojeva u beskonacnoj petlji ˇ
sve dok korisnik ne upiše „Done“ (bez navodnika). Pri tome brojeve spremajte u listu. Nakon toga
potrebno je ispisati koliko brojeva je korisnik unio, njihovu srednju, minimalnu i maksimalnu
vrijednost. Sortirajte listu i ispišite je na ekran. Dodatno: osigurajte program od pogrešnog unosa
(npr. slovo umjesto brojke) na nacin da program zanemari taj unos i ispiše odgovaraju ˇ cu poruku. """

newlist=[]
while True:
    
    try:
        broj=input()
        if broj == 'Done': break
        newlist += [int(broj)]
    except:
        print("Pogresan unos")

print(newlist)
print(len(newlist))

avg=0
min=int(newlist[0])
max=int(newlist[0])
for broj in range(len(newlist)):
    if min>newlist[int(broj)]:
        min=newlist[int(broj)]
    if max<newlist[int(broj)]:
        max=newlist[int(broj)]
    avg+=int(newlist[broj])
avg=avg/len(newlist)

print(min)
print(max)
print(avg)

newlist.sort()
print(newlist)
