""" Napišite program koji od korisnika zahtijeva upis jednog broja koji predstavlja
nekakvu ocjenu i nalazi se izmedu 0.0 i 1.0. Ispišite kojoj kategoriji pripada ocjena na temelju ¯
sljedecih uvjeta:
>= 0.9 A
>= 0.8 B
>= 0.7 C
>= 0.6 D
< 0.6 F
Ako korisnik nije utipkao broj, ispišite na ekran poruku o grešci (koristite try i except naredbe).
Takoder, ako je broj izvan intervala [0.0 i 1.0] potrebno je ispisati odgovaraju ¯ cu poruku. """

print("Unesite ocijenu: ")

try:
    ocijena=float(input())
    if ocijena >= 0.9 and ocijena <= 1.0:
        print('A')
    elif ocijena >= 0.8 and ocijena < 0.9:
        print('B')
    elif ocijena >= 0.7 and ocijena < 0.8:  
        print('C')
    elif ocijena >= 0.6 and ocijena < 0.7:  
        print('D')
    elif ocijena >= 0.0 and ocijena < 0.6:  
        print('F')  
    else:
        print('Nedozvoljen interval')    
except:
    print('Nije unesena ocijena')
