import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt("osu_lv\osu_lv2\data.csv", delimiter=",", dtype=str)
print(data)

print("Number of people:")
print(len(data[:]))

height = data[1:, 1].astype(np.float_)
print(height)
weight = data[1:, 2].astype(np.float_)

#b)
plt.scatter(height, weight, c="b", s=1)
plt.xlabel('x os')
plt.ylabel('y os')
plt.show()

#c)
height_50th = data[1::50,1].astype(np.float_)
print(height_50th)
weight_50th = data[1::50,2].astype(np.float_)
plt.scatter(height_50th, weight_50th, c="b", s=1)
plt.xlabel('x os')
plt.ylabel('y os')
plt.show()

#d)
min_height = min(data[1:, 1])
print("Minimal height:", np.min(height))
print("Maximal height:", np.max(height))
print("Average height:", np.mean(height))

#e)
male = (data[1:,0] == '1.0')
female = (data[1:,0] == '0.0')

male = data[np.where(male)]
m=male[1:, 1].astype(np.float_)

female = data[np.where(female)]
female = female[1:, 1].astype(np.float_)
print("Minimal male height:", np.min(m))
print("Minimal female height:", np.min(female))

print("Maximal height:", np.max(m))
print("Maximal height:", np.max(female))

print("Average height:", np.mean(m))
print("Average height:", np.mean(female))
