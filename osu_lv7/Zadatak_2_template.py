import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as Image
from sklearn.cluster import KMeans


# ucitaj sliku
img = Image.imread(r"osu_lv\osu_lv7\imgs\test_1.jpg")

# prikazi originalnu sliku
plt.figure()
plt.title("Originalna slika")
plt.imshow(img)
plt.tight_layout()
plt.show()

# pretvori vrijednosti elemenata slike u raspon 0 do 1
img = img.astype(np.float64) / 255

# transfromiraj sliku u 2D numpy polje (jedan red su RGB komponente elementa slike)
w,h,d = img.shape
img_array = np.reshape(img, (w*h, d))

# rezultatna slika
img_array_aprox = img_array.copy()

colors = len(np.unique(img_array_aprox, axis=0))
print('Number of colors in original picture:', colors)


criteria = (Image.TERM_CRITERIA_EPS + Image.TERM_CRITERIA_MAX_ITER, 100, 0.2)
_, labels, (centers) = Image.kmeans(img_array, 3, None, criteria, 10, Image.KMEANS_RANDOM_CENTERS)
centers = np.uint8(centers)
labels = labels.flatten()
segmented_image = centers[labels.flatten()]
segmented_image = segmented_image.reshape(img.shape)
plt.imshow(segmented_image)
plt.show()


km = KMeans(n_clusters=5, init='random', n_init=5, random_state=0)
km.fit(img_array_aprox)
labels = km.predict(img_array_aprox)
plt.figure()
plt.imshow()
plt.title("Result:")
plt.tight_layout()
plt.show()

